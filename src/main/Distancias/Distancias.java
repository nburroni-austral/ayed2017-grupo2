package main.Distancias;

/**
 * Created by Tomas on 21/3/2017.
 */
public class Distancias {
    /**
     *
     * @param firstWord the first word which is going to be compared, must have the same length that the second.
     * @param secondWord the second word which is going to be compared, must have the same length that the first.
     * @return the minimum amount of substitutions required to reach form the first word to the second.
     */
    public int hamming(String firstWord, String secondWord){
        int result = 0;
        if (firstWord.length() != secondWord.length())
            throw new RuntimeException("Hamming's method only works with two Strings with the same length.");
        for (int i = 0; i < firstWord.length(); i++) {
            if (firstWord.charAt(i) != secondWord.charAt(i))
                result++;
        }

        return result;
    }

    /**
     * This method creates a multi array using the first word as columns and the second as rows. Then it fills the first row
     * with the first word (starting at i = 1) and the first column (again starting at i = 1). Then it starts filling each
     * position at the multi array following the next condition. It compares the 3 values which are stored at the left of
     * the position we are currently on(matrix[i-1][j]), the one above the same position (matrix[i][j-1]), and the one above
     * and left from the same position(matrix[i-1][j-1]). If either the one at the left or the one above are the minimum it adds
     * one to the value from that position and assigns it to matrix[i][j](matrix[i][j] = matrix[i-1][j]+1 or matrix[i][j-1]+1).
     * If the minimum is the other option it takes its values and adds to it a 0 if that letter is the same as the position we
     * are standing on, or a 1 if it's different. The value at the last position is the distance between both words.
     *
     * @param firstWord the first word which is going to be compared, must have the same length that the second.
     * @param secondWord the second word which is going to be compared, must have the same length that the first.
     * @return the minimum amount of operations required to reach from the first word to the second.
     * Those operations being: substitution, insertion, deletion.
     */
    public int levenshteinDistance(String firstWord, String secondWord) {
        int result = 0;
        int lengthOfFirstWord = firstWord.length();
        int lengthOfSecondWord = secondWord.length();
        char[] firstWordArray = firstWord.toLowerCase().toCharArray();
        char[] secondWordArray = secondWord.toLowerCase().toCharArray();
        int[][] matrixOfLevenshtein = new int[lengthOfFirstWord + 1][lengthOfSecondWord + 1];

        if (lengthOfFirstWord == 0) {
            return lengthOfSecondWord;

        } if (lengthOfSecondWord == 0) {
            return lengthOfFirstWord;

        } if (firstWord.equals(secondWord)){
            return 0;
        }

        for (int i = 0 ; i < lengthOfFirstWord ; i++) {
            matrixOfLevenshtein[i][0] = i;
        }

        for (int j = 0; j < lengthOfSecondWord; j++) {
            matrixOfLevenshtein[0][j] = j;
        }

        for (int i = 1 ; i <= lengthOfFirstWord; i++) {
            for (int j = 1 ; j <= lengthOfSecondWord; j++) {
                int deleteOperation = matrixOfLevenshtein[i-1][j] + 1 ;
                int insertionOperation = matrixOfLevenshtein[i][j-1] + 1 ;
                int substitutionOperation = matrixOfLevenshtein[i-1][j-1];
                if (firstWordArray[i-1] != secondWordArray[i-1]) {
                    substitutionOperation += 1 ;
                }
                matrixOfLevenshtein[i][j] = Math.min(Math.min(deleteOperation , insertionOperation) , substitutionOperation);
                result = matrixOfLevenshtein[i][j];
            }
        }
        return result;
    }
}
