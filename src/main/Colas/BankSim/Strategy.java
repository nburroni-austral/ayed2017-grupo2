package main.Colas.BankSim;


import main.Colas.DynamicQueue;

/**
 * Created by DiegoMancini on 11/4/17.
 */
public interface Strategy {

    void goStrategyA(BankRTC aBank);

    void gostrategyB(BankRTC aBank);

    boolean isUsingStrategyA();

    boolean isUsingStrategyB();

    void useStrategy(BankRTC bankRTC , DynamicQueue<Client> clientsQueue);
}
